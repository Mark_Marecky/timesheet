﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TimeTTTracker.Models;

namespace TimeTTTracker.Controllers
{
    public class ProjectController : Controller
    {
        ApplicationDbContext _context;

        public ProjectController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        public ActionResult Index()
        {
            var projekty = _context.Projects.ToList();
            return View(projekty);
            //return View();
        }
    }
}