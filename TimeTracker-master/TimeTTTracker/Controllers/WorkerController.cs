﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TimeTTTracker.Models;

namespace TimeTTTracker.Controllers
{
    public class WorkerController : Controller
    {
        ApplicationDbContext _context;

        public WorkerController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        public ActionResult Index()
        {
            var pracownicy = _context.Workers.ToList();
            return View(pracownicy);
        }

        //[Authorize(Roles = "CanRegister")]
        //public ActionResult Register(Worker worker)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        var newWorker = new Worker
        //        {
        //            Imie = worker.Imie,
        //            Nazwisko = worker.Nazwisko,
        //            Email = worker.Email,
        //            Password = "zaw2ZAW@",
        //            Uprawnienia = worker.Uprawnienia
        //        };

        //        _context.Workers.Add(newWorker);
        //    }
        //    return View();
        //}

        public ActionResult Login()
        {
            return View();
        }
    }
}