﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TimeTTTracker.Startup))]
namespace TimeTTTracker
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
