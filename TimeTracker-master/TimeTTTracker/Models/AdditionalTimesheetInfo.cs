﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NodaTime;

namespace TimeTTTracker.Models
{
    public class AdditionalTimesheetInfo
    {
        public int Id { get; set; }

        public Timesheet TimeSheet { get; set; }

        public DateTime BeginDate { get; set; }

        public TimeSpan Hours { get; set; }

        public int Weeks { get; set; }
    }
}