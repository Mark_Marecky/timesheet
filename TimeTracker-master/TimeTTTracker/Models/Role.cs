﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TimeTTTracker.Models
{
    public class Role
    {
        public int Id { get; set; }
        public string Stanowisko { get; set; }
    }
}