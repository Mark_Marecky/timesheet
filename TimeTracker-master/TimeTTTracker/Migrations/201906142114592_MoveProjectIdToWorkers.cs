namespace TimeTTTracker.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MoveProjectIdToWorkers : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Workers", "AdditionalTimesheetInfo_Id", "dbo.AdditionalTimesheetInfoes");
            DropIndex("dbo.Workers", new[] { "AdditionalTimesheetInfo_Id" });
            DropColumn("dbo.Workers", "AdditionalTimesheetInfo_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Workers", "AdditionalTimesheetInfo_Id", c => c.Int());
            CreateIndex("dbo.Workers", "AdditionalTimesheetInfo_Id");
            AddForeignKey("dbo.Workers", "AdditionalTimesheetInfo_Id", "dbo.AdditionalTimesheetInfoes", "Id");
        }
    }
}
